import wtforms
from models import Author, Genre, Song


class SongForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.DataRequired()])
    duration = wtforms.StringField(validators=[
        wtforms.validators.Regexp("\d{2}:\d{2}", message="Wrong duration format"),
        wtforms.validators.DataRequired()
    ])
    author = wtforms.SelectField(
        choices=[(x.id, x.name) for x in Author.select()]
    )
    genre = wtforms.SelectField(
        choices=[(x.id, x.name) for x in Genre.select()]
    )

    def save(self):
        Song.create(name=self.name.data,
                    duration=self.duration.data,
                    author=self.author.data,
                    genre=self.genre.data)
