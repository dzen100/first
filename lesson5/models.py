from peewee import SqliteDatabase, Model, AutoField, CharField, TextField, ForeignKeyField


db = SqliteDatabase("new_db.sqlite3")


class Author(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)
    description = TextField(null=True)

    class Meta:
        database = db


class Genre(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)

    class Meta:
        database = db


class Song(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)
    duration = CharField(max_length=5)
    author = ForeignKeyField(Author, backref='songs')
    genre = ForeignKeyField(Genre, backref='songs')

    class Meta:
        database = db


if __name__ == '__main__':
    db.create_tables([Author, Genre, Song])
